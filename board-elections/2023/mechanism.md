# How Will I Vote?

The election organizer will send out detailed instructions on voting
on November 5 2023, including the list of valid candidates.  The
process is described below:

You'll vote with a text file.  The content and the author of any vote
will be public.  At some point in the text file you include a magic
line with only the following string:

    === 2023 keys.openpgp.org Board approvals ===

(this magic line includes the equals signs, is case-insensitive, and
ignores leading and trailing whitespace)

Below that line, you write the name of each candidate that you approve
of, one per line.  We'll strip leading and trailing whitespace, but
please try to keep everything else the way the candidate announced
themselves so we don't have to play full-blown Unicode normalization
games.  We'll ignore any lines that don't match one of the declared
candidates.

You can approve of ("vote for") every and any candidate that you want
to, by listing them on the ballot.  There is no limit.

You'll cast your ballot by sending it, cryptographically signed, to
the election organizer (for this election, that's Daniel Kahn Gillmor
<dkg@fifthhorseman.net>).  During this voting period, the organizer
will post regular summaries of who they have received readable ballots
from to the `koo-voting@enigmail.net` mailing list.  If they can't
make sense of your ballot, they'll let you know.

If you want to enhance the ability to break ties in the election (see
"Breaking Ties" below), you can also instead send the organizer just
the OpenPGP signature of your ballot before the election concludes.
The organizer will publish all the signed ballots (and, for those who
submitted them, dangling signatures) at the end of the election.

If you submitted a dangling signature, you'll have 24 hours after the
end of the election to send the ballot itself, which the organizer
will then confirm against the signature.  Note that if you send a
dangling signature, the organizer cannot verify that your ballot is
readable until after it is too late for you to correct it.

If you later send another ballot, the organizer will use the last one
that they receive before the voting ends, sorted based on the OpenPGP
timestamp in the signature.  A later ballot completely replaces any
approvals from the earlier ballots.

24 hours after the voting ends, the organizer will publish all the
signed ballots and the rank order of selected candidates that will be
used for the process of filling the Board.

# How Is The Board Filled?

There are five Board seats.  Each candidate that received at least
five approvals is eligible to fill a Board seat.  We will fill the
seats starting from the most-approved candidates.

The ordered list of candidates ranked by number of approvals, with
ties broken as described in "Breaking Ties" below.

If any selected candidate declines to join the Board, they will be
skipped and the next candidate on the list will be offered the seat.

Note that some candidates *should* decline a Board seat when offered.
In addition to people who find they need to bow out for logistical
reasons, we want a diverse board, because the community is diverse.
If the Board looks like it's starting to be lots of your team already,
you should probably consider declining this time around to make sure
other teams are also represented.  The KOO Constitution (see
`constitution.md`) explicitly says:

> * When offered a seat on a new Board, a candidate is expected to
>   decline the seat if they feel their affiliations are already
>   well-represented on the Board being assembled.

Note that in an asynchronous, remote election, each selected candidate
may need some time to accept or reject a seat, so this might take some
time.  If you are a candidate, you'll have at least 24 hours to
confirm before I move on, but please watch your mail during this
crucial week if you're a candidate who received five or more
approvals.  If you are offered a seat and you do not send a
cryptographically-signed acceptance 24 hours after the offer, you will
be skipped.

If you are selected and want to decline, you can speed up the process
for everyone else by sending me a cryptographically-signed rejection.

On November 26th, along with the names on the newly assembled Board,
the organizer will publish a simple textual summary of the process by
which we arrived at the result.

The Board begins its new term on December 1, 2023.

# Breaking Ties

In the 2023 Board election, there are fewer candidates than Board
seats, so it's unlikely that ties will be relevant.  That said, if any
need arises for breaking ties, we will use the same process as the
last cycle.

One problem with approval voting with a relatively small electorate is
that sometimes there are ties.  See `tie-breaking.md` for more details
and rationale, but for this election, we will do the following:

For each valid and contributing ballot B, at most one per voting
member:

 - Verify the signature of B
 - let T be the bytestream representing the OpenPGP-canonical textual
   form of B (without the signature)
 - let H(T) = SHA2_256("KOO Board 2022 sort order:" || T)

Sort the ballots B by H(T) (hash 00...0 comes first, hash ff...f comes
last), and concatenate the ballots themselves into one large text
document X.

Where "hex" means "lower-case hexadecimal ASCII representation",
calculate a seed string:

    S = hex(SHA2_256(X))

For a candidate identified by textual label N, each candidate gets a
tie-breaking position:

    Z(N) = SHA2_256(S || N)

If two candidates A and B have the same number of approvals, the one
with a lower Z(N) will be earlier than the other in the ordered list.

# Conclusion

If you read this far, thanks for the interest in the details!  If you
have concerns, suggestions, or comments, please follow up!

# List Of All Voting Members For 2023 KOO Board Election

- 01F2D39B501512458599AA849AC10CB104AFC954 Patrick De Schutter
- 0A292B5F8A3C247F586F19D7E1AF518CC4B1DC35 Kristof (GPGTools)
- 0FB1803BC91D1B601E2A2C22702A26DB4CCB3FFC Floris Bruynooghe
- 1480E7DA19D210E097BAE6A2566C91A4D2E84A76 sva
- 29B3336B457807F947C1EAD64DC6EB56822AE846 Dominik Schürmann
- 2DA81D01455C3A0032198850F305447AF806D46B juga
- 341EA32E0D2FD84DC6DBE04BF03913F6BD0FFAD9 Björn Petersen
- 473A25E3F3945F227BD70CD3153FCAA0234B7663 Dr. Stavros Kousidis
- 4F9798C6278E744B8C61B325DD40F258AACE01E9 Bernd R. Fix
- 4F9F89F5505AC1D1A260631CDB1187B9DD5F693B Patrick Brunschwig
- 5D031F1D410B980FBC006E3202C134D079701934 Thomas Oberndörfer
- 5FA6AE7533D6BA546B89B220E058DF4FEBEF28AC Eduardo Vela Nava
- 608B00ABE1DAA3501C5FF91AE58271326F9F4937 Lukas Pitschl
- 61A015763D28D410A87B197328191D9B3B4199B4 Bjarni Runar Einarsson
- 645BA27B926EB3061934DED22139A3CF1DFD7A16 Christian Zier
- 689F48FD61B72A12467C255A74D72D477BF66E37 Bart Butler
- 6A46F0864D3A4FE54175EA25CCACCA376089CC87 samba
- 72E33AE81300E553BC4EEDEFCB064A128FA90686 Daniel Huigens
- 769B651054DB697FEB26E408717574BD0A6591ED paz
- 7B4578C7D0122BF76B5EE5DD6786A150F6A2B28F Ola Bini
- 7F9116FEA90A5983936C7CFAA027DB2F3E1E118A Paul Schaub
- 8975A9B33AA37910385C5308ADEF768480316BDA Kevin J. McCarthy
- 8B95B2515F6BBB6AB6149D557E6761563EFE3930 Aron Wussler
- 8F17777118A33DDA9BA48E62AACB3243630052D9 Neal H. Walfield
- 91B6FC0116719D175EA3B54E0BAD489530DBB025 Moritz Bartl
- 9AD6C740FE4E043ECB7D5220729B88D4095E8E13 Ronald Evers
- 9EBDC46AB510F90921DB84B2DD28EE5AE4783280 Heiko Stamer
- A367FA346081EBF7A962E8DF0DB42E9C6FE9B68D Andy Mueller-Maguhn
- AB7937F5C93E891387038D69367297B7C12A035B Mohammad Salman Nadeem
- B2B397904D39F3B3D4BA511EA5E6BCA629BA4127 drebs
- BD859D3F3BEE87DB687C4D61D6339DD31F9F3EC2 Nami Lefherz
- C29F8A0C01F35E34D816AA5CE092EB3A5CA10DBA Daniel Kahn Gillmor
- CBCD8F030588653EEDD7E2659B7DD433F254904A Justus Winter
- D4AB192964F76A7F8F8A9B357BD18320DEADFA11 Vincent Breitmoser
- DB546519B22089EFDBE8130940DC6189F9269749 Kai Engert
- DBE5439D97D8262664A1B01844E17740B8611E9C Lars Wirzenius
- DDEBA5D26F64DC406368799F2A4BEC40729185DD Lara Bruseghini
- E1BE98DB1DB8469491B864AC595AF2E4F4188531 Christoph Klünter
- E59ECD5A30913721291A9983D4E582E1C253B035 Marin Thiercelin
- F98D03D7DC630399AAA6F43826B3F39189C397F6 Tobias Mueller
