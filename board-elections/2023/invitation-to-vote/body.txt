Dear NAME,

The 2023 voting for the keys.openpgp.org Board starts now.

Please vote by mailing dkg@fifthhorseman.net a signed message with the
list of candidates you approve for the board.  Even though this is not
a hotly contested election, please vote!  The KOO voting body uses the
act of voting to ensure that its members are alive and engaged (see
"the voting body" in `constitution.md`)

The simplest way to vote is to just hit reply, and remove everyone in
the quoted/attributed text that you don't want to approve of. You can
approve of as many as you like.  If you self-nominated, you can also
vote for yourself.

Please send me a signed mail with your votes before November 19th
(2023-11-19 23:59:59 UTC)

You can also vote by sending me a dangling signature by the same date,
but you'll need to send me the corresponding ballot within 24 hours
after the vote itself was due.

If you want more details about the electoral mechanism, see:

  https://gitlab.com/keys.openpgp.org/governance/-/blob/main/board-elections/2023/mechanism.md

Please remember to sign your message with your OpenPGP certificate
with fingerprint FINGERPRINT.

Below is the list (ordered by first name) of all candidates for
the election.

=== 2023 keys.openpgp.org Board approvals ===
Daniel Huigens
Lukas Pitschl
Ola Bini
Vincent Breitmoser
