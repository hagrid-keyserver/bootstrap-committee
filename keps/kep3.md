Kep 3: Signing HTTP responses

# Motivation

Some applications (such as Proton Mail) want to request keys from keys.openpgp.org via a proxy (such as Proton's servers), in order to hide the identity of the user, and to enable caching of responses on the proxy.
However, this opens up the possibility of a MITM attack, where the proxy can serve any key, including one for which it controls the private key.
In this case, it would be beneficial to sign responses, so that they can be verified by the client.

# Proposed solution

We propose to add experimental support for serving [Signed HTTP Exchanges (SXG)][SXG] to keys.openpgp.org, by installing and enabling the [NGINX SXG module][NGINX-SXG].

When requested by the client, the NGINX SXG module will convert the response from KOO into a Signed HTTP Exchange, using the key provided to the module.

The responses can then be verified by the client, either using a hardcoded certificate, or by verifying the certificate chain.

Since SXG is still an IETF draft, rather than an RFC, this functionality should be considered experimental and subject to change without notice. We will not proactively recommend the use of this functionality.

## Alternatives considered

### Certifying the User ID

While not a direct alternative, another possibility could be to sign/certify the User ID in the key, or create a User ID packet and certify it. This has the advantage that it can be verified using standard OpenPGP tooling, but gives weaker guarantees (for example, it would allow a proxy to remove revocation signatures and other parts of the key, without invalidating the User ID certification).

This alternative is also to some extent orthogonal, and could still be done after implementing this proposal.

### Signing HTTP Messages

There is an alternative IETF draft for [Signing HTTP Messages](https://www.ietf.org/archive/id/draft-ietf-httpbis-message-signatures-04.html), which seems to serve a similar use case to [SXG][SXG]. However, SXG seems to have better support in NGINX, and will thus be easier to deploy.

### Group Signatures

Rather than signing individual certificates, KOO could (perhaps periodically) create a "group signature" of all certificates, and selectively reveal "proofs" that the signature signs a given certificate (without revealing other certificates that have been signed), for example using [BBS Signatures](https://www.ietf.org/archive/id/draft-irtf-cfrg-bbs-signatures-01.html).

This may be faster than signing all certificates individually, though subsequent updates may be slower.

Depending on the implementation, this has the downside that there may be a lag in certificate updates, or a lag in signing (i.e. shortly after updating a certificate, either the old certificate would be served, or the new certificate would be served without a signature). This lag would presumably be determined by the performance characteristics of the BBS Signature Scheme, which should be investigated if we want to go with this option.

It would also likely be much more work to implement.

### Key Transparency

Similarly, KOO could periodically place all certificates in a Merkle Tree, and sign the tree root. The tree root could additionally be publicized by placing it in a blockchain or Certificate Transparency logs (by requesting a TLS certificate for <merkle-tree-root>.keys.openpgp.org, for example). Or, equivalently, KOO could publicize a BBS group signature in this way.

This has the additional advantage that clients could verify that at any point in time, KOO is serving the same certificate to all clients. If the certificate owner additionally verifies that their own key is correctly included in every published Merkle Tree (accounting for key updates, of course), then any malicious key updates will be detectable.

Furthermore, this solution is resistant to collusion between KOO and the proxy.

As above, one downside is that there would either be a lag in certificate updates, or in the availability of a Merkle Tree inclusion proof for a recently updated certificate. And, again this option would be a lot more work to implement.

### Other Ideas

Rather than requiring signed responses, clients could send requests via TOR, a VPN, a non-TLS-terminating proxy, or [Oblivious HTTP](https://www.ietf.org/archive/id/draft-thomson-http-oblivious-00.html).

However, these options would have the downside that certificates can't be cached by the proxy, placing additional load on KOO to serve the same certificate to multiple users. This also doesn't offer additional privacy from the proxy if the proxy also acts as the mail server (as in Proton's case), as the mail server already knows which email addresses the user is communicating with.

# Specific work needed

(See also <https://web.dev/articles/how-to-set-up-signed-http-exchanges>.)

1. Generate a self-signed certificate
2. Install and configure the NGINX SXG module
3. Test the above functionality (both manual testing and ideally, if practical, adding automated tests)

# Who will make those changes?

The operations team is kindly requested to execute steps 1 and 2 above.
Step 3 will be done by Proton.

# Known tradeoffs

## How does the change interact with the stated privacy policy?

The proposal does not interact with [the current privacy policy](https://keys.openpgp.org/about/privacy).

## What additional resource consumption costs do we expect from the change?

Additional CPU resources will be required to sign responses, when requested. As a rough upper bound, this is expected to (at most) double the CPU time required to handle a given response, given that TLS is also currently used, and it is expected that SXG would have a roughly similar impact on performance. However, further investigation would be needed to provide a clearer estimate, if this were to be a concern.

## What changes for the user stories?

The existing user stories do not change, however, this proposal would additionally enable securely requesting a certificate from keys.openpgp.org via a proxy.

## What additional duties does this KEP impose on the operations team?

In addition to the initial work required, the operations team is expected to keep the private key safe, and ideally (in the future) set up a Shamir's Secret Sharing of the key material (as well as other keys relevant to the operation of KOO) with the board.

# Rollback plans

If issues arise, the NGINX SXG module can be disabled by the operations team.

[SXG]: https://www.ietf.org/archive/id/draft-yasskin-http-origin-signed-responses-09.html
[NGINX-SXG]: https://github.com/google/nginx-sxg-module
