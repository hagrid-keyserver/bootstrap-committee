# KOO Board Meeting - 2025-01-10

Present: Andrew, Heiko, Daniel

- Andrew is now secretary

- Daniel volunteers to handle voting for next board election (since he will not be eligible to run again)

# Summary (Daniel)

- We're effectively missing a full Ops team (Vincent has been doing the work, but his availability is limited, and he is interested in handing off [some of] the responsibility)

- Daniel has offered to spend some of his/Proton time for implementing things (but probably not running things)

- Board sets goals (taking input from voting body), ops team manages implementation (both dev and ops)

# Work queue

The board enumerated the outstanding items remaining from previous years, and agreed to begin prioritising them for the next meeting:

- Initiate a review of existing stale proposals, e.g. Hagrid PRs

- Ask for clarity on support level for RFC9580 in sq library, before starting work on v6 support in hagrid

- Review the options for v6 support:
    1. Support multiple keys in current api version (by "splitting" the service into two parts for v4 and v6) [KEP-1]
    2. Bump api version for multiple keys [KEP-2]
    3. Only serve multiple keys in v1 HKP [ should we draw up a KEP-3 for this? ]

- Need also to consider UX of multiple-key verification

    - Should this also require encrypted challenges?

    - Enigmail WKS integration was a good model to follow

    - Updating two keys (e.g. with equivalency between v4 and v6) should be atomic

- Check on progress of sqlite back-end

- Consider a Shamir-shared vault containing the operations secrets in case of emergency

- Consider possibility of a backup instance and/or HA cluster

    - Full deployment instructions should be documented

- Should we roll out the logo on koo website? :)

# action items

1. Daniel: Ask Vincent to update the board email alias (board@keys.openpgp.org/io)

2. Andrew: Make new pgp key and share with the board members, publish "board2025" public key

3. Daniel: Update info about board on GitLab and KOO site

4. Andrew: Create matrix channel for board+ops

5. Everyone: Try to get volunteers for ops team

# decisions

- Meet every two weeks for now (until workload reduces)
- Next meeting Fri 24 Jan @1400Z
- Think about prioritisation in interim

## housekeeping

- chat for informal discussion
- pads for agenda bashing
- meetings for in-depth problem resolution and formal decisions
