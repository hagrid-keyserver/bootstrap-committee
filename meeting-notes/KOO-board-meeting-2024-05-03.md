# KOO Board Meeting - 2024-05-03

Present: Daniel, Ola, Vincent

* Revisit action items
  [ ] Vincent: Ask eclips.is if they can authorize board members to perform support flows
  [ ] Vincent: Set up SXG experimentally on testing2.keys.openpgp.org
    * on hold, while feature is unclear at proton
  [x] Vincent: (optional) rebase sqlite branch and check it out
  [x] Daniel: Ask the mailing list members who are not voting body members if they want to be voting body members, and make it explicit that their membership would be public
  [x] Daniel: Move the member list to governance repo
  [x] Daniel: Ask Patrick about mailing lists

Vincent: more time constrained now, might not have as much time for meetings in the future


* Operational update

** didn't manage to meet with devan (tried once but failed due to timezone shenanigans)
** sqlite branch ("hagrid 2.0") is finished, tested, benchmarked, and working
*** could deploy with it
**** vote: 3 in favor, 0 against, 1 absent


* Voting body membership of Andrew Gallagher
*** vote: 3 in favor, 0 against, 1 absent

* Membership request of an unknown person
** will ask if they want to join the voting body

* Proton KOO lookups update
** summary of previous discussion:
*** discussed ui and flows of proton and gpgtools for macos
*** should first agree whether publishing keys on KOO should be used automatically for encryption
**** what about signing only keys or something?
**** should require user involvement?
*** should clarify purpose of KOO?

** Vincent: tend to agree that automatic encryption requires a stronger signal than a verified key on the internet (e.g. koo or wkd)

** Signaling mechanism specific to KOO?

*** ui to enable/manage on KOO
**** periodic email to keep enabled?
**** single email to enable?
**** just a switch in the UI?

*** signal for clients?:
**** armored header
**** unhashed subpacket
**** http header
**** endpoint flag



* Potential UI improvements? Sending test emails?

(didn't get to this)


Action Items
  [ ] Vincent: Add DNS records to point lists.keys.openpgp.org to Hostpoint
  [ ] Daniel: Add Andrew Gallagher to the voting body member list in the governance repo
  [ ] Daniel: Ask voting body about signalling for automatically encrypting emails (and sending emails?)
    Post-meeting discussion: will poll usage first.
