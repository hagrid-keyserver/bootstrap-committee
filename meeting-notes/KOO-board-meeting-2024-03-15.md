# KOO Board Meeting - 2024-03-15

Present:  Daniel, Lukas, Ola, Vincent

* Revisit action items
  [x] Daniel: ask dkg about subscribed people not in the GitLab repo
  [x] Daniel: Setup private repo to collect the email addresses of the voting body as canonical source of truth
  [x] Daniel: Merge MR 27
  [ ] Vincent: Ask eclips.is if they can authorize board members to perform support flows
    Didn't get to any koo related stuff since the last meeting
  [x] Daniel: send notes to mailing list


* Operational update

Nothing to report, other than Proton update below


* Proton KOO lookups update (if there's interest?)

** At 50% requests, load is fine
** Feedback from some users: Proton used keys from KOO, but user intended to use a different one
** Other than that, worked fine
** For the future: deploy some privacy preserving lookup mechanism (bloom filter)


* Discuss membership of people subscribed to the mailing list who are not in the voting body

** create a koo-discuss list for public discourse?
*** open registration -> easier to engage
*** could make the the voting@ archive subscriber-only

** D: move from hostpoint.ch to something of our own?
** probably not needed
** but could use our own DNS, e.g. voting@ and/or discuss@lists.keys.openpgp.org


* Public list of voting body members?

** keeping the list secret makes voting easier to manipulate
** it was public so far, and noone complained
** let's keep it like this. put it in governance repo then, too


* Where to go in terms of Hagrid dev

** good first step: finish the sqlite (or other databsae backend) branch
** would unlock features like bloom filter above, simpler backup story, support flows
** no concrete plans on who's gonna do it and how, but that would probably be a good next step


* neighbourhoodie improvement project

** no updates from their side yet (meeting was on 09.02.)
** V: pinged them about this again


Action Items:

[ ] Vincent: Ask eclips.is if they can authorize board members to perform support flows
[ ] Vincent: Set up SXG experimentally on testing2.keys.openpgp.org
[ ] Vincent: (optional) rebase sqlite branch and check it out
[ ] Daniel: Ask the mailing list members who are not voting body members if they want to be voting body members, and make it explicit that their membership would be public
[ ] Daniel: Move the member list to governance repo
[ ] Daniel: Ask Patrick about mailing lists
