# KOO Board Meeting 2023-09-15

## Attendees
- Ola
- Vincent
- Neal
- Lukas

Absent with excuse:
- Daniel


## Operations Update
* KOO Outage - wasn't able to trace the technical reason
* Alerting worked - notified at 2.30am
* Machine died
* Someone created a ticket
* It's unclear what happened
* Currently the machine can't reboot itself if it dies
* Vincent will notify the board next time, if something like this happens
* Neal: Do we have a Mastodon account where we could post about incidents - some people asked what has happened
* Vincent: Maybe a status board would make sense
* Currently out of scope


## Voting Process KOO Board
* Vincent: 
  * Motivation to work on KOO is waning
  * how much I care keeps to diminish over time
  * should pass on everything while motivation still lasts
  * we should think more concretely how to replace me
  * if there's no way to replace me, we will shutdown the service eventually
  * will set some kind of timeline (6 month from now)
  * we could make call into the voting body if anyone is interested

* Decision to not actively recruit people to the voting body.  But people are still welcome to submit an "application" to the board if they want to join.

* Ask trusted third-party if they could run the election again since Lukas nominating himself (as secretary) would not be a conflict?

* Vote:
  * Public ballot as last year
  * Find 2 trusted third-parties who could and publish the end result. No posting of the official ballots.  Otherwise the same process as last year.
  * Unanimous decision of present board members for option two (secret ballot).


## Actions
* Vincent: Will draft an email for voting body to ask if anyone is up to taking over KOO development and operations
* Lukas: Will write email to trusted third-parties to ask if they could be counting and publish the end results


# Next Meeting on September 29th 2023