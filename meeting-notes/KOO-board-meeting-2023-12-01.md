# KOO Board Meeting - 2023-12-01

Present: Ola, Daniel, Neal, Lukas, Vincent

## Agenda:

* Redesign of KOO website
* Swearing in of the new board
* Election of a secretary
* Ops update
* Progress with Devan

## Redesign of KOO website
* Sequoia got accepted to received some design services / website development services
* Neal had a meeting with Neighbourhoodie (company in Berlin, JS/Website contracting)
* They suggested they could do a re-design / CI for sequoia as well as KOO
* Neal clarified that KOO is no longer under the Sequoia umbrella and that he will bring it up with the board


Discussion:
* Vincent: i think the design is generally still fine. Improvements could be made to the "CMS"/build system
* Vincent: If they could integrate hugo, it could be nice if that could be integrated for the for example About pages / blog pages
* Daniel: I am not against them analyzing and suggest design improvements. Maybe they could work on a logo (based on a logo which was first mocked up by Proton)
* Vincent: we could also have them work on openpgp.org
* Vincent: So it would be great if they could add hugo integration
* Daniel: I could spend some time with them to talk about what is needed. Could be point of contact to discuss the KOO website
* Neal: I can do the handoff
* Vincent: Daniel and I should schedule a meeting with them an see if we can find common ground


## Progress with Devan
* Vincent connected with Devan and proposed a plan.
* Devan is currently travelling. Should start next few day
* Will start with a second instance
* The second instance should then take over the original one
* Started freshening up the ansible packages
* Lukas: Devan should document the process along the way


## Swearing in of the new board
* Thank neal for his service
* Vincent: update board@ email to new board
* Rotate encryption subkey for board@
** Revoke previous encryption subkey, add new, with expiry slightly more than 1 year

## Election of a secretary
Proposal: Daniel should be secretary. Vincent should be backup secretary
Vote:
Lukas: Yes
Vincent: Yes
Ola: Yes
Daniel: Yes (Fine, whatever)
Daniel delegates note taking responsibilities to Vincent


## KOO Logo
* proton contributed logo. this happened a year ago, before board was formed and never came up over the year
* Daniel sent KOO logo to board@
* We'll vote on the logo later once the new board is sworn in
* Voting for order of preference:
    Lukas: A, B, C
    Ola: B, C, A
    Daniel: A, B, C
    Vincent: A, B, C
* Voting on whether we like it better than the current text?:
    unanimously yes
* Voting on whether to change the background:
    unanimously keep current


## Ops update
* someone reported trouble sending an email to support@, will check
* see above for devan updates


## brief discussion on where to meet
* keep using jitsi? move to zoom?
* sticking with jitsi for now


## Actions
* Neal: will contact Neighbourhoodie and cc it to the board and tell them about what we discussed.
* Vincent: Rotate board key
* Daniel: Get logo design files
