Present: Daniel, Lukas, Neal, Ola,
Absent: 
Absent with excuses:

# Agenda

* Discuss Neils proposal of changes to KOO's privacy notice


# Discuss Neils proposal of changes to KOO's privacy notice

(See appendix/2023-05-23-keys.openpgp.org-privacy-notice-thoughts.odt)

* Add a note under the "Public Data" paragraph, that while anyone can upload a public key, the associated user ID data is only exposed, if the user ID is specifically verified.
* Request processing
* Mention the retention period of the request logs which might include PII (email address)
* In the future we might hash the email instead of sending it in plain
* Should we leave the paragraph in that a key deletion request can be requested via support@keys.openpgp.org
* What to do with revoked keys? How do you proof that you are the owner of the key in that case?
* Vincent: How do you proof that you are the owner of the key?
* Neal: we could use revocation reasons
* Vincent: they are probably not as clear as they should be.
* Vincent: maybe we should leave it in and adapt if requested too often
* Daniel: If you can verify your email address that should be enough to proof ownership
* Neal: This is a difficult situation if the key was compromised since under attack the email account could be compromised as well. Revocation certificate is also not tied to the email address / UID but to the primary key
* Daniel: If we keep the UIDs for revoked key that might be more of an issue, but if we removed them, maybe it's not that much of an issue
* Vincent: Mabye let's leave it in and see what happens. No objections.
* Save Neil's document as an appendix to the meeting notes


# OpenPGP Email Summit and effect to KOO
* Daniel: it was discussed how to handle V6 keys
* KOO should support both types of keys
* it should be possible to specifically request a V6 key

# Backlog on KOO
* Neal: there's a merge request for hagrid that addresses a vulnerability in sequoia


# Action Items

[ ] Vincent: will formulate the discussed changes and create a merge request which can then be reviewed by Neil
[ ] Neal: will ask Neil if we can save the document and add as an appendix. And if he's fine if we credit him for it.

# Next meeting on 23rd June, 2023
