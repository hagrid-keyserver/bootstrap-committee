Present: Ola, Neal, Daniel, Lukas
Absent: Vincent
Absent with excuses:

# Agenda

* Discuss action items from previous meeting and their progress
* Discuss voting body application by Bjarni Runar Einarsson


# Action Items from previous meeting

- [x] Vincent: Setup board@keys.openpgp.org and forwarding to all board members
- [x] Vincent creates board key, distributes it to board members with revocation cert
- [x] Neal: Provide script to setup key pair
- [ ] Lukas: look into available CoC's and talk to people who might have more experience


## Blocked by member acceptance process


- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Vincent: (possibly as part of onboarding operations team member?) document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
- [ ] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues
  
  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over
- [ ] Create MR for the public facing documentation of the process how to become a member of the voting body
- [x] Lukas: add "Operation teams report" as default agenda point
- [ ] Daniel: Formulate the process how to request a rate limit increase
- [x] Daniel: Write a KEP for KOO signing User IDs


# Voting body application

* Accept Bjarni's application:
    Neal: Yes
    Daniel: Yes
    Ola: Yes
    Lukas: Yes
* We welcome Bjarni to the voting body
* Inform Bjarni of successful application


# Actions items for next meeting on Friday, March 24th
- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Vincent: (possibly as part of onboarding operations team member?) document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
- [ ] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues
  
  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over
- [ ] Create MR for the public facing documentation of the process how to become a member of the voting body
- [ ] Daniel: Formulate the process how to request a rate limit increase
