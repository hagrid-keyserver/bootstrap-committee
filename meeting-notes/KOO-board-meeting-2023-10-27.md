# KOO Board Meeting 2023-10-27

## Attendees

- Neal
- Vincent
- Daniel
- Ola
- Lukas

Absent: 


## Agenda

* Operations update
* Discuss Heiko's application + vote
* Update from Vincent regarding pass over of KOO operations
* Election status
* Preparing amendment to the constitution for secret ballots


## Operations update
* Nothing happened


## Update from Vincent regarding passover of KOO operations
* Had a call with Devan
* Seems competent and has an idea what he is going into
* Vincent gave general overview to Devan of how things work and are deployed
* Didn't go into the weeds yet
* For now no next meeting is scheduled (Vincent on vacation)
* Plan is to let Devan change different parts of the system to get a better understanding of the stack
* Ola: It would be great if he could spin up some KOO replicas on VPSs
* Vincent: He could spinup a version of the backup
* Lukas: Document the steps along the way to make onboarding easier in the future
* Devan also had good ideas how monitoring could be improved

* Devan will be paid through money from STF/Sequoia – he will work for Sequoia
* Conflict of interest?
* Vincent: Devan must be independent in his work and his superior must be the KOO board
* Daniel: Both, KOO board and Sequoia should approve the work. If Sequoia doesn't want to sponsor some project he's not obligated to do it.
* Ola: Sequoia could write a document where they state that they donate some percentage of money to work on KOO operations
* Vincent: One exmaple could be that Sequoia could veto features by withdrawing money
* Daniel: We could also setup a legal entity for the board that then organizes funding. Question is if it's worth the work.
* Vincent is pretty confident that Devan will work out


## Discuss Heiko's application
Vote:
    * Ola: Yes
    * Daniel: Yes
    * Vincent: Yes
    * Neal: Yes
    * Lukas: Yes
* Accepted!
* How to proceed from here? Will Patrick add him to the official list?
* Lukas: Will he be able to participate in the voting this time?
* Ola: It's a bit weird that we are in charge of adding people to the voting body that can vote for us
* Daniel: He can't vote since he applied after the official election announncement. That's a bit weird, since the announcement is the perfect moment to apply
* Ola: If the mailing list doesn't align with the voting body that would be a problem
* Lukas: The secretary should have access to the mailing list


## Election status

* Applications:
    * Ola
    * Daniel
    * Vincent
    * Lukas

* No application yet from Neal
* No further applications from non-KOO board members 


## Preparing amendment to the constitution for secret ballots and align with current practices

* Vincent: Maybe the board can change it and voting body can veto instead
* Daniel: If we make such a change it will also go through the voting process. It might look sketchy if the first thing we change is voting process.
* Neal: First step should be to simply align the constitution with the current process
* Changes:
  * Instead of merge requests a signed email to the election organizer should suffice
  * Election organizer will publish the results at the end 



## Actions

- [ ] Lukas: Tell Heiko he is now part of the voting body.
- [ ] Lukas: Ask Patrick for access to the mailing list
- [ ] Daniel: Write a merge request for the constitutional changes


# Next Meeting: November 10 2023