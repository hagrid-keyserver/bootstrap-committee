Present: 
Absent: 
Absent with excuses:

# Agenda

* Discuss action items from previous meeting and their progress
* Operations team report
* Discuss how GDPR might affect KOO with Expert


# Discuss action items from previous meeting and their progress
- [ ] Lukas: look into available CoC's and talk to people who might have more experience (ask at openpgp-email summit)
- [x] Neal: Invite Expert to talk about GDPR and keyservers. Either next regular meeting or based on offer by Expert (on UK time). To make it easier to find a date, only a sub group will meet if the date doesn't work for everyone.
- [ ] Vincent: Add a sentence how to join a voting body to the news item introducing the board (they should write to board@keys.openpgp.org)
- [ ] Vincent + Lukas: discuss and document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
  -> Vincent will ask again, who was interested in doing it before. If they don't have time soon, Lukas and Vincent will find some time in May. (ask at openpgp-email summit)
- [x] Vincent: lookup stats how many requests / second/hour KOO receives approximately to determine if a fallback might make sense. How many read operations / how many write operations
- [ ] Vincent: send an email to the voting body mentioning that this should be discussed at the OpenPGP Email Summit


# Operations team report
* Update mail server to no longer store bounces
* At the beginning bounces and out of office emails were stored
* Since just a bunch of useless data, it is no longer stored.


* Question from Lukas: why not encrypt the verification email?
** simplicity - we don't do any kind of private key operations on the server
** attack scenario unclear (not doing it allows an attacker to upload keys that they *don't* control)
** some keys might not be capable of decryption

** Idea: Educational
** by encrypting / signing the verification emails, users could test if their OpenPGP setup is working 
** -> should probably be an independent service


# Discuss how GDPR might affect KOO with Expert
* Currently not sure about compliance with GDPR
* OpenPGP certificate data is more likely personal data than not. It's probably not PII, but GDPR talks of personal data which is broader than PII.
** contact@company would not be personal data, since it doesn't relate to any specific person. however, this is not possible to distinguish in practice
* Vincent gave a rough explanation of key discovery (lookup of keys by email, upload requires verification) and key distribution (lookup of keys by fingerprint, no verification for upload)
* also, based on historical expectations of SKS
* Looking up by user id (key discovery) is pretty good: choice to opt in, choice to opt out again
* Key distribution: good reason for maintaining it the way that it has been done.
** Not confident that it's not personal data
** risk that someone will object seems low, no objections in the last four years.
** No need to change this immediately.
* Could be more transparent in privacy notice. Doesn't appear to be extremely urgent to update though.
* Expert will suggest changes to the current privacy notice and share via cryptpad
* Expert: reassured that we haven't had any complaints. Can't rule out a bad faith attack.

### Aside: Sharing key updates between servers?

* Neal: Future idea could be to distribute subkey updates to other key servers. How would the privacy notice have to adapted for that?
  * Instead of distributing to all key servers, it might be considered to only distribute between the most popular ones and try to formulate a privacy notice that those share, which allow for a similar expectation of privacy
  * It could be done on a consent basis, so make it opt-in instead of all keys or none 
  * Vincent: Can't share certificate updates, because our privacy policy says we don't allow enumeration.
  * Expert: We could change the policy. The question is: what is the policy implication.
  * Vincent: Doesn't matter if we don't send User IDs. Third-parties could just fetch the certificates to get the User IDs. Even with the rate limiting. (Only 1000s of updates per day.)

# Action items for next meeting: June 9th, 2023
- [ ] Lukas: look into available CoC's and talk to people who might have more experience (ask at openpgp-email summit)
- [ ] Vincent: Add a sentence how to join a voting body to the news item introducing the board (they should write to board@keys.openpgp.org)
- [ ] Vincent + Lukas: discuss and document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
  -> Vincent will ask again, who was interested in doing it before. If they don't have time soon, Lukas and Vincent will find some time in May.
- [ ] Vincent: send an email to the voting body mentioning that this should be discussed at the OpenPGP Email Summit
