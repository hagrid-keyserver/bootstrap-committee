# KOO Board Meeting - 2025-01-24

Present: Andrew, Heiko, Daniel

## Matters arising from previous minutes

### Action items

* create board email alias (done)
* create board pgp key (done)
* update website info (we are waiting for a fuller writeup)
* create matrix channel (done)
* ask for more ops help (announcement made on voting list)

### Other low hanging fruit

* sqlite back end migration (done)
* update website logo (will batch with other website updates)
* get sq support timeline for 9580 (awaiting confirmation)

### Prioritisation of backlog

The Board decided on the following list of high-level priorities for 2025:

1. sqlite backend rollout (already done)
2. implementing v6
3. review open PRs
4. certification improvements (1pa3pc etc.)

### V6 implementation

Andrew proposed to write up KEP-4:

* HKPv1 should serve multiple keys, but restrict them to the ones that the keyholder indicates. The most preferred key should be first.
* The UX for how the keyholder submits and approves these keys will be similar to KEP-2.

(post-meeting note: this became https://gitlab.com/keys.openpgp.org/governance/-/merge_requests/36/diffs)

## Action items

* Daniel: get writeup and logo for website
* Heiko: ask sequoia about timeline for 9580 and PQC support
* Heiko: ask Vincent about current back end choice (openssl)
* Heiko: Would the currently used openssl backend be OK for PQC?
* Andrew: get shamir's secret + vault, and make sure all access has a backup person or vault key/credentials
* Heiko: discuss with ops team about ops documentation
* All: Close out sqlite back end task (ask ops team for official confirmation/status report; how do revoked keys work, are ones without user ids fine from the new sqlite data source?)
* Andrew: write up KEP-4

Next meeting Friday 21st February
