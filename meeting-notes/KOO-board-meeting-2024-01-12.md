# KOO Board Meeting - 2024-01-12

Present: Daniel, Lukas, Ola, Vincent

## Agenda:

* Operational update
* Vote on nomination of Georg Faerber to the voting body
  * Daniel, Lukas, Ola, Vincent in favor

* Neighbourhood.ie; should we ping them?
  * Yes, Daniel will ping them

* Meta-discussion on organizing KEPs
  * Keep as-is

* Vote on options for V6:
  1. KEP-1: Simple v6 compatible API [1]
  2. KEP-2: Support for multiple keys [2]
  3. Deprecate our custom API (in favor of HKP), and don't add v6 support
  4. Don't do anything for now

  * Roughly favor of KEP-1
  * Unclear situation with v4/libre/v6, we'll wait a while longer

* Discussion (and vote?) on KEP-3: Signing HTTP Responses [3]

  * Vincent: due to Mozilla's opposing of SXG, seems to be a stagnant technology
  * Vote: Lukas, Ola, Vincent vote against adoption, Daniel for adoption

~~* Discussion on privacy-preserving lookups, e.g. OHTTP (perhaps only if we have time?)~~


[1]: https://gitlab.com/keys.openpgp.org/governance/-/merge_requests/21
[2]: https://gitlab.com/keys.openpgp.org/governance/-/merge_requests/22
[3]: https://gitlab.com/keys.openpgp.org/governance/-/merge_requests/27
