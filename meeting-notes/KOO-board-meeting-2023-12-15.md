# KOO Board Meeting - 2023-12-15

Present: Ola, Daniel, Lukas, Vincent

## Agenda:

* Follow up on action items from last time
** Rotate board key: Send out new encryption subkey to everyone, then publish new certificate
** Sent new key to board members
* Ops update
** small spike in support requests in the last two weeks, but none indicative of bugs/issues on our side
* Progress with Devan
** none yet

* Vote on open KEPs / proposals?

  * V4 / V6
** proposals on the table:
    - simple kep-1 by vincent
    - slightly more involved kep-2 by daniel
    - wait for hkp effort from working group
** should we vote on it? or how to proceed?
** V: should we have an action plan?
*** D: Proton might dedicate some time to it
*** D: might be fine without action plan, deciding just on the direction of the board
*** V: sounds reasonable

  * Signing keys / responses
** L: do you have a preference?
*** D: simple approach via nginx SGX requests signing (suggested by vincent), just enable request signing with nginx, or signing uids directly via openpgp (suggested by neal). no strong preference for now, probably simple approach
*** O: against turning koo into a CA (on openpgp level), prefer to go as non-openpgp as possible
*** L: different use cases not necessarily competing
*** V: should think about users, too. who gonna use it (openpgp signing: openpgp folks perhaps, requests signing: proton)
**** D: we would use requests signing

*** V: drawback of request signing commits us to compatible http
*** O: actually, drawback is management of key?
**** L: reuse tls cert?
**** O: key reuse for different purposes not a good idea

** ...some detailed discussion on key management..
** D: seems like we want to focus on SXG first. I can write a KEP on that
** general agreement

* voting in general

** V: don't feel competent on this call to vote on either without prior notice, it's been months since I looked at either. do it later?
*** D: then let's vote on next time
*** ok

** D: does an ordered vote of preference make sense?
*** no disagreement

## next meeting:
* next suitable day: 12.01.2024 due to holidays


## Actions from last time
* Neal: will contact Neighbourhoodie and cc it to the board and tell them about what we discussed.
** answered on Tuesday, waiting for their response
* Vincent: Rotate board key
** done
* Daniel: Get logo design files
** done
