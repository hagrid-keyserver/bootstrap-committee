# KOO Board Meeting - 2024-02-23

Present: Daniel, Lukas, Vincent, Ola

* Revisit action items
  [ ] Daniel: ask dkg about subscribed people not in the GitLab repo
    didn't happen
  [ ] Setup private repo to collect the email addresses of the voting body as canonical source of truth
    We forgot to assign, so it didn't happen :)
  [x] Vincent: Check out SXG module
    * Works fine, and compiles with newer version of nginx (1.24.0)
    * Few dependencies, so likely won't break
      * even though unmaintained
  [x] Daniel: update KEP-3
    * Done, updated to indicate its experimental status


* Voting to adopt KEP-3 in its new experimental version
** All in favor
-> merge MR 27: https://gitlab.com/keys.openpgp.org/governance/-/merge_requests/27

* Operational update
** eclips.is answered about account management, said there was an old account as well. removed that account, will proceed to set one up for devan
** Lukas: could we tell them a contact email for support purposes in the future?
*** V: Likely possible, I'll ask them about it. Ideally this would be a kind of fallback support way to access the infrastructure
*** V: Maybe to keep complexity low on the eclipsis side, instead just add accounts one or more board members? that access can only indirectly access the running machines, so probably ok from an opsec PoV

Conclusion for now:
Ask eclipsis about it, if it's no problem use support flow. Otherwise give account to one or more board members.

* Discussion on privacy-preserving lookups, e.g. OHTTP
** D: Related to thread tarted on the openpgp ml by Kai Engert a short while ago
** D: should briefly discuss if we want to help with that?
** V: for context, we already have a tor hidden service up and running for this purpose
** V: is OHTTP widely deployed? how do users use it?
   D: some deployment by apple, and cloudflare. but not sure how widely it is deployed
** V: should wait until at least some client expresses intent to ship
** D: we wouldn't even have to do anything, a relay could work independently
** V: a relay *must* be independently operated and governed for the concept to make sense, so we couldn't do this purely from our side

* D: Should we send the meeting notes to the voting list?
* -> yes

* L: was there an update about devan?
* V: yes, see above. slowly, though
* D: please try before parental leave
* V: will do

Action Items:
  [ ] Daniel: ask dkg about subscribed people not in the GitLab repo
  [ ] Daniel: Setup private repo to collect the email addresses of the voting body as canonical source of truth
  [ ] Daniel: Merge MR 27
  [ ] Vincent: Ask eclips.is if they can authorize board members to perform support flows
  [ ] Daniel: send notes to mailing list
