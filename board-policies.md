# Board Policies for KOO

This document describes standard procedures and policies for the KOO Board for the purposes of continuity between instantiations of the Board and transparency to the broader KOO community.

The Board may change these procedures and policies by usual Board action.

In the event that the constitution conflicts with this document, the constitution takes precedence.

## Every Board member maintains an OpenPGP certificate

Each Board member needs to be able to communicate securely with other Board members and people in the broader OpenPGP community.

The Board member's known OpenPGP certificate should be encryption-capable and signing-capable for until at least one month after the end of their current term on the Board.

The certificates of the active members of the Board shall be maintained in a publicly-visible location, and published on `keys.openpgp.org`.

## Standard Board decision-making

As the constitution says, Board decisions are typically made by consensus.
All Board decisions are reported publicly in minutes of Board activity.

When a decision cannot be made by consensus, any Board member may put a proposal to a vote within the Board.
Such a decision must be framed as a clear text proposal, capable of being approved or rejected by a yes or no choice.
Neither the proposal itself, nor the ballots cast on such a proposal are secret.

Any Board member may abstain from voting on any such proposal for any reason.
Any Board member is expected to recuse themself from (and therefore abstain from voting on) a proposal where they have a conflict of interest.
A Proposal is approved by the Board if and only if a simple majority of Board votes cast are in favor of the Proposal.

Any vote proposed (even votes on proposals that were not ultimately approved) is also be reported publicly in minutes of Board activity.
This reporting includes the votes cast by each member of the Board, unambiguously attributed.

## Dealing with sensitive data

In some cases, the Board may have access to sensitive data.
For example, they may field reports from the operations team that contain sensitive information.

Under no circumstances should any member of the Board reveal sensitive information that would breach the Privacy Policy of the KOO service.
When sensitive information is available to the Board, the Board MAY decide to disclose it outside the Board (or publish it as public data) as a standard Board decision as long as it does not breach the Privacy Policy.

The Board SHOULD indicate to the public that it has access to any information that it bases any decision on, though that indication should not reveal the specific sensitive information.
For example, the Board might indicate possession of "the material redacted from the Operations Team report from July 2022".

When membership in the Board changes, the previous Board makes available the sensitive information it has to all members of the incoming Board.
Outgoing members of the Board destroy any copies they have of sensitive Board information.

The Board may at any time, as a standard Board decision, decide to destroy specific sensitive information that it has access to.
When this happens, all members of the Board destroy any copies they have of that specific sensitive information.

## Regular meetings

The Board meets synchronously six times a year, roughly at two month intervals.

Meetings take place on whatever synchronous platform is acceptable to all members of the Board.

A week before a meeting takes place, the secretary sends out a reminder to all Board members including the time and coordinates for the meeting.

If a Board member cannot attend a meeting, they let the other Board members know in advance, and propose alternate times that they would be able to attend. The meeting may still be held, unless objected to by the absent board member.

Meeting notes are published promptly after each meeting by the secretary.

## Vetting new candidates for the voting body

When a voting member nominates a new individual for inclusion as a voting member, the Board is expected to consider the candidate.

If the candidate appears to be applying in good faith, but has not provided sufficient information to tell whether they are active in helping the OpenPGP ecosystem, the Board should ask the candidate for more information about their work and plans, either publicly or privately.

If the candidate provides followup information to the Board about themself that they need to be private, the Board should treat that information as sensitive data.

## Considering proposals for enhancing KOO 

Members of the community may ask a Board member to adopt a KOO Enhancement Proposal (a.k.a. KEP) to shepherd it through the board decision-making process.

See KEP-outline.md for the sorts of things the Board should consider in a KEP.

For ease of discussion, any Board member who adopts a KEP should assign it an as-yet-unassigned number, and ensure that it has a clear, concise, and unambiguous title.

## Deciding on a secretary

Some critical Board operations cannot function without an active secretary.
For example, the followup Board cannot be elected if there is no secretary.

In the event that the Board cannot come to consensus on who should play the role of secretary, Board members should agree to selection of a secretary by chance among every Board member willing to serve the role (see tie-breaking.md for some reasonable mechanisms).

In the event that no Board member indicates willingness to serve as secretary, the Board should assign the role by chance among all Board members.

## The KOO mailing list

There should be a publicly archived mailing list for all voting members to participate in.
This is known as the "KOO mailing list".
The Board maintains this mailing list and ensures that it is open to all voting members.

Voting members are expected to be subscribed to the KOO mailing list, though any member may disable message delivery if they prefer.

People who are not voting members cannot subscribe or post (though of course they can read the public archives of the list).

In the event that activity on the mailing list becomes problematic for the organization for any reason, the Board may impose posting restrictions for a limited time.

The Board may delegate the duties of mailing list supervision to any member of the voting body by standard Board decision.
Such a delegation remains in force (including across Board transitions) until it is explicitly revoked.

## Maintenance of public records

The ongoing process of governing KOO produces records, including reports from the Operations team, Board minutes, the history of proposed KEPs, electoral history, the KOO mailing list, and so on.

The Board should ensure that these public records are easily available to the general, anonymous public for at least 10 years from the date of record creation.
