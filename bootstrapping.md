### Initial formation of the voting body and the Board

* Membership in the initial voting body is open to anyone who has
  attended any of the past [OpenPGP E-mail
  Summits](https://wiki.gnupg.org/OpenPGPEmailSummits).  The Summit
  attendance requirement only applies to the first round of voting
  member candidates, and shall not apply to future voting member
  candidates.
  - _Rationale:_ This is meant to mechanically bootstrap an initial
    voting body that's aware of and cares about the OpenPGP ecosystem.
    Requiring summit attendance for initial candidates is intended to
    prevent a malicious take-over or lengthy disputes at the outset by
    applying a simple historical rule.  We recognize that this simple
    rule is biased and non-representative of the global community we
    want to participate: for example, people who find it challenging
    to travel to or within Europe are less likely to have attended a
    summit.  To start correcting this bias as soon as possible, we
    expect the initial voting body to propose (and the first board to
    promptly approve) membership of interested and active members of
    the OpenPGP community who have not been able to attend a Summit in
    the past.
* There will be an initial period of four weeks for people interested
  in being part of the initial voting body to nominate themselves by
  posting a message to the summit mailing list at
  `openpgp-email@enigmail.net`.  Interested people should identify
  themselves with an OpenPGP certificate, to facilitate future voting.
  - _Rationale:_ We need a place to assemble membership, and people
    need time to be able to step forward.
* In case there is any dispute over identity or historical attendance,
  Patrick Brunschwig will adjudicate who is eligible for membership in
  the initial voting body.
  - Hopefully there won't be any challenges to matters of identity or
    historical fact.  If there is a challenge, though, we want a clear
    way to resolve conflicts.  Patrick is a longstanding organizer of
    the summits, an active and respected leader in the OpenPGP
    community, and is willing to deal with disputes in this case.
* After the initial voting body is assembled, the election of the
  initial board will begin, with Patrick Brunschwig as the organizer.
  This election will also be announced on the Summit mailing list,
  if no KOO mailing list has been established yet.
  - _Rationale:_ Each election needs an organizer, but we have no
    Secretary of the Board for the initial Board election.  We are
    leaning on Patrick again here.
* To avoid having the whole Board be required to step down at once,
  the initial Board election will assign two of the selected Board
  members to start with a two-year limit.  The remaining three initial
  Board members will start with the standard three-year limit.  If the
  selected Board members all agree on who gets the two-year limit,
  that decision holds.  If they cannot agree, the two two-year members
  will be selected by chance.
  - _Rationale:_ It'd be unfortunate to have to replace the whole
    initial Board at once due to term limits.
* The constitution will be considered ratified upon successful
  election of the first Board.
  - _Rationale:_ This seems the simplest process of ratification.

